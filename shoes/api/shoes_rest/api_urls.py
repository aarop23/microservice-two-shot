from django.urls import path

from .views import (
    api_shoe_list,
    api_shoes,


)

urlpatterns = [
    path("shoes/", api_shoe_list, name="api_list_shoes"),
    path("shoes/<int:id>/", api_shoes, name="api_shoe"),
]