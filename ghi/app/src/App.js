import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ShoesList from "./ShoesList";
import ShoesForm from "./ShoesForm";
import Nav from './Nav';

function App() {
 
console.log("Hello")
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />}></Route>
          <Route path="/shoes" element={<ShoesList />}></Route>
          <Route path="/shoes/new" element={<ShoesForm />}></Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
