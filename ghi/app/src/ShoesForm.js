import React, { useEffect, useState } from 'react';

function ShoesForm() {
    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState('');
    const [model_name, setModel_name] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPicture_url] = useState('');
    const [bin, setBin] = useState('');


    const handleNameChange = (event) => {
        const value = event.target.value;
        setModel_name(value);
    }
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture_url(value);
    }
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    // handle submit form
    const handleSubmit = async (event) => {
        event.preventDefault();
        // create an empty JSON object
        const data = {};
        data.manufacturer = manufacturer;
        data.model_name = model_name;
        data.color = color;
        data.picture_url = picture_url;
        data.bin = bin;
        console.log(data);

        const BinUrl = `http://localhost:8080/api/shoes/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(BinUrl, fetchConfig);
        if (response.ok) {
            const newBin = await response.json();
            console.log(newBin);

            setManufacturer('');
            setModel_name('');
            setColor('');
            setPicture_url('');
            setBin('');
        }
    }

    
        const fetchData = async () => {
            const url = 'http://localhost:8100/api/bins/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setBins(data.bins);
                console.log(data.bins)
            }
        }
    

        useEffect(() => {
        fetchData();
    }, []);


        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new Shoe location</h1>
                        <form onSubmit={handleSubmit} id="create-shoe-bin-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleNameChange} placeholder="Model_name" value={model_name} required type="text" name="model_name" id="model_name" className="form-control" />
                                <label htmlFor="model_name">Model name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleManufacturerChange} placeholder="manufacturer" value={manufacturer} required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                                <label htmlFor="room_count">Manufacturer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleColorChange} placeholder="color" value={color} required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handlePictureChange} placeholder="picture_url" value={picture_url} required type="URL" name="picture_url" id="picture_url" className="form-control" />
                                <label htmlFor="picture_url">Picture url</label>
                            </div>
                            <div className="mb-3">
                                <select required name="bin" id="bin" onChange={handleBinChange} value={bin} className="form-select">
                                    <option value="">Choose a bin</option>
                                    {bins.map(bin => {
                                        return (
                                            <option key={bin.href} value={bin.id}>
                                                {bin.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button type="submit" className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    
}
export default ShoesForm;

