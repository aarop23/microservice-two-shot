import React, { useState, useEffect } from "react";

function ShoesList() {
    const [shoes, setShoes]=useState([])
    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
            console.log(data.shoes)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = async (e) => {
        const url = `http://localhost:8080/api/shoes/${e.target.id}`

        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const response = await fetch(url, fetchConfigs)

        if (response.ok) {
            fetchData();
        } else {
            alert("Hat was not deleted");
        }
    };




    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model name</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Closet Name</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map((shoes) => {
                    return (
                        <tr key={shoes.id}>
                            <td>{shoes.manufacturer}</td>
                            <td>{shoes.model_name}</td>
                            <td>{shoes.color}</td>
                            <td>{shoes.picture_url}</td>
                            <td>{shoes.Bin}</td>
                            <td><button onClick={handleDelete} id={shoes.id} className="btn btn-danger">Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ShoesList;

